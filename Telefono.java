import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class Telefono extends JFrame {

	private JTextField numTessera2;
	private JLabel numTessera1;
	private JButton conferma, annulla;

	private Container container;
	private GridBagLayout tabella;
	private GridBagConstraints constraints;

	public Telefono () {

		super ("Numero di tessera");

		container = getContentPane();
		tabella = new GridBagLayout ();
		container.setLayout(tabella);
		constraints = new GridBagConstraints();

		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 1000;
		constraints.weighty = 1000;

		numTessera1 = new JLabel("   Inserire il Numero di Tessera ");              // FISICAMENTE CORRISPONDE ALLA CONSEGNA DELLA TESSERA E ALLA LETTURA DELLA STESSA TRAMITE UN LETTORE DI TESSERE (LA TESSERA E' MAGNETICA)
		numTessera1.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		numTessera2 = new JTextField("");
		numTessera2.setEditable(true);
		numTessera2.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		conferma = new JButton("Conferma");
		conferma.setFont(new Font( "Serif", Font.PLAIN, 15 ) );

		annulla = new JButton("Annulla");
		annulla.setFont(new Font( "Serif", Font.PLAIN, 15 ) );

		// ADDCOMPONENT

		addComponent (numTessera1,       0, 0, 2, 1);
		addComponent (numTessera2,       0, 2, 2, 1);
		addComponent (annulla,           1, 2, 1, 1);
		addComponent (conferma,            1, 3, 1, 1);

		// EVENTI



		annulla.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						setVisible( false ); 
					}
				}
				);

		conferma.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						boolean can = true;
						int n = 0;
						if (!( numTessera2.getText().equals(""))) {
							try {
								n = Integer.parseInt(numTessera2.getText());
							}
							catch (java.lang.NumberFormatException e) {
								JOptionPane.showMessageDialog( Telefono.this, "Errore nella compilazione!", "", JOptionPane.PLAIN_MESSAGE);
								can = false;
							}
							if (can) {
								if (n < Libreria.numClienti) {
									for (int i = 0; i < Libreria.numClienti; i++) {
										if (Libreria.clienti[i].numTessera == n) {
											JOptionPane.showMessageDialog( Telefono.this, "Numero di Telefono del Cliente:  " + Libreria.clienti[i].numTel, "", JOptionPane.PLAIN_MESSAGE);
											setVisible(false);
										}
									}
								}
								else {
									JOptionPane.showMessageDialog( Telefono.this, "Il cliente non esiste!", "", JOptionPane.PLAIN_MESSAGE);
                                    numTessera2.setText("");
								}
							}
						}


						else {
							JOptionPane.showMessageDialog( Telefono.this, "Errore nella compilazione!", "Operazione non riuscita", JOptionPane.PLAIN_MESSAGE);
						}

					}
				}
				);

		setResizable( false );
		setSize( 700, 100 );
		setLocationRelativeTo(null);
		setVisible( true );

	}   // END CONSTRUCTOR

	//METODO ADDCOMPONENT

	private void addComponent( Component component, int row, int column, int width, int height ) {

		constraints.gridx = column;
		constraints.gridy = row;

		constraints.gridwidth = width;
		constraints.gridheight = height;

		tabella.setConstraints( component, constraints );
		container.add( component ); 
	}


} // END CLASS