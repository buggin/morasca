import java.awt.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.*;

public class Display extends JFrame {

	private static JMenuBar bar;
	private static JMenu cliente, libri, aggiungiLibro, info;
	private static JMenuItem telefono, rifornimento, nuovo, usato, restituzione, infoNuovo, infoUsato;
	protected static JMenuItem logout, registrazione, accedi;
	protected static JTextField titolo2;
	protected static JLabel libri1, titolo1, disponibile, prezzo;
	private static String names[] = {"Acquisto", "Prestito"};
	private static JRadioButton scelta[];
	private static ButtonGroup gruppo;
	protected static JButton conferma;

	protected static JLabel iconaLogo, iconaLucchetto;
	private static Icon logo = new ImageIcon("Logo.png");
	private static Icon lucchetto = new ImageIcon("lucchetto.gif");
	protected static Icon cartello = new ImageIcon("pericolo.png");

	private static boolean acquista, fine;
	protected static Nuovo libroincorso;
	protected static Usato usatoincorso;
	protected static Cliente clienteincorso;        // CONTIENE IL CLIENTE CHE HA EFFETTUATO L'ACCESSO E CHE STA ESEGUENDO L'ACQUISTO (O LA PRENOTAZIONE)

	private static int numLibriVenduti;  // PER RESOCONTO FINALE DELLA GIORNATA
	private static double ricavato;

	private static Container container;
	private static GridBagLayout tabella;
	private static GridBagConstraints constraints;

	// COSTRUTTORE 

	public Display () {

		super ("Libreria Insubria");


		container = getContentPane();
		tabella = new GridBagLayout ();
		container.setLayout(tabella);
		constraints = new GridBagConstraints();

		// BARRA IN ALTO

		bar = new JMenuBar();
		setJMenuBar(bar);

		cliente = new JMenu("Cliente");                       // SEZIONE CLIENTE
		bar.add(cliente);

		registrazione = new JMenuItem("Registra un Cliente");
		cliente.add(registrazione);
		KeyStroke key = KeyStroke.getKeyStroke(KeyEvent.VK_R, KeyEvent.CTRL_DOWN_MASK);
		registrazione.setAccelerator(key);

		accedi = new JMenuItem("Accedi");
		cliente.add(accedi);
		KeyStroke key1 = KeyStroke.getKeyStroke(KeyEvent.VK_A, KeyEvent.CTRL_DOWN_MASK);
		accedi.setAccelerator(key1);

		telefono = new JMenuItem("Telefono Cliente");
		cliente.add(telefono);
		KeyStroke key2 = KeyStroke.getKeyStroke(KeyEvent.VK_T, KeyEvent.CTRL_DOWN_MASK);
		telefono.setAccelerator(key2);

		logout = new JMenuItem("Logout");
		cliente.add(logout);
		logout.setVisible(false);
		KeyStroke key3 = KeyStroke.getKeyStroke(KeyEvent.VK_L, KeyEvent.CTRL_DOWN_MASK);
		logout.setAccelerator(key3);


		libri = new JMenu("Libri");                       // SEZIONE LIBRI
		bar.add(libri);

		info = new JMenu("Informazioni libro");
		libri.add(info);

		infoNuovo = new JMenuItem("Nuovo");
		info.add(infoNuovo);
		KeyStroke key4 = KeyStroke.getKeyStroke(KeyEvent.VK_I, KeyEvent.CTRL_DOWN_MASK);
		infoNuovo.setAccelerator(key4);

		infoUsato = new JMenuItem("Usato");
		info.add(infoUsato);
		KeyStroke key5 = KeyStroke.getKeyStroke(KeyEvent.VK_BACK_SPACE, KeyEvent.SHIFT_DOWN_MASK);
		infoUsato.setAccelerator(key5);


		aggiungiLibro = new JMenu("Aggiungi Libro");
		libri.add(aggiungiLibro);

		rifornimento = new JMenuItem("Rifornimento arrivato");
		libri.add(rifornimento);
		KeyStroke key9 = KeyStroke.getKeyStroke(KeyEvent.VK_R, KeyEvent.VK_CANCEL);
		rifornimento.setAccelerator(key9);

		nuovo = new JMenuItem("Nuovo");
		aggiungiLibro.add(nuovo);
		KeyStroke key6 = KeyStroke.getKeyStroke(KeyEvent.VK_1, KeyEvent.CTRL_DOWN_MASK);
		nuovo.setAccelerator(key6);

		usato = new JMenuItem("Usato");
		aggiungiLibro.add(usato);
		KeyStroke key7 = KeyStroke.getKeyStroke(KeyEvent.VK_COMMA, KeyEvent.SHIFT_DOWN_MASK);
		usato.setAccelerator(key7);


		restituzione = new JMenuItem("Restituzione Libro");
		libri.add(restituzione);
		KeyStroke key8 = KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, KeyEvent.SHIFT_DOWN_MASK);
		restituzione.setAccelerator(key8);

		// JTEXTFIELD

		prezzo = new JLabel(" Prezzo : ");
		prezzo.setFont(new Font( "Serif", Font.PLAIN, 20 ) );


		libri1 = new JLabel(" Libri al prossimo sconto: ");
		libri1.setFont(new Font( "Serif", Font.PLAIN, 20 ) );
		libri1.setVisible(false);



		titolo1 = new JLabel(" Inserisci il titolo e premi Invio:");
		titolo1.setFont(new Font( "Serif", Font.PLAIN, 20 ) );



		titolo2 = new JTextField("");              
		titolo2.setEditable(true);
		titolo2.setFont(new Font( "Serif", Font.PLAIN, 20 ) );



		disponibile = new JLabel(" Disponibile?");
		disponibile.setFont(new Font( "Serif", Font.PLAIN, 20 ) );







		// JRADIOBUTTON

		ItemHandler handler = new ItemHandler();

		scelta = new JRadioButton[ names.length ];
		for ( int count = 0; count < names.length; count++ ) {
			scelta[ count ] = new JRadioButton( names[ count ] );
			scelta[ count ].addActionListener( handler );
		}


		gruppo = new ButtonGroup();

		for (int i = 0; i < names.length; i++)  {
			gruppo.add(scelta[i]);
		}

		scelta[0].setSelected(true);
		acquista = true;
		
		

		// JBUTTON

		conferma = new JButton(" Conferma ");
		conferma.setFont(new Font( "Serif", Font.PLAIN, 15 ) );
		


		// LOGO

		iconaLogo = new JLabel(logo);

		iconaLucchetto = new JLabel(lucchetto);
		iconaLucchetto.setText("");
		iconaLucchetto.setVisible(false);



		// ADDCOMPONENT

		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 1000/2;
		constraints.weighty = 1000/2;



		addComponent (iconaLogo,         0, 0, 10, 2);
		addComponent (scelta[0],         3, 0, 1, 1);
		addComponent (scelta[1],         3, 1, 1, 1);
		addComponent( iconaLucchetto,    3, 7, 2, 1);
		addComponent (titolo1,           4, 0, 3, 1);
		addComponent (titolo2,           4, 1, 10, 1);
		addComponent (prezzo,            5, 0, 4, 1);
		addComponent (disponibile,       6, 0, 4, 1);
		addComponent (libri1,            7, 0, 4, 1);
		addComponent (conferma,          8, 8, 1, 1);


		// EVENTI


		// BARRA IN ALTO - SEZIONE LIBRI



		infoNuovo.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {

						InfoLibro application1 = new InfoLibro();
					}
				}
				);

		infoUsato.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						InfoUsato application1 = new InfoUsato();
					}
				}
				);



		nuovo.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {

						AggiungiNuovo application1 = new AggiungiNuovo();

					}
				}
				);


		usato.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {

						if (clienteincorso == null) {
							Accedi application1 = new Accedi(0);

						}
						else {

							AggiungiUsato application1 = new AggiungiUsato();
						}
					}
				}
				);

		rifornimento.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {

						Rifornimento application1 = new Rifornimento();
					}
				}
				);

		restituzione.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {

						Restituzione application1 = new Restituzione();
					}
				}
				);

		registrazione.addActionListener(                                    // LA REGISTRAZIONE CONSISTE SOLAMENTE NEL CONSEGNARE UNA TESSERA AL CLIENTE 
				// CARATTERIZZATA DA UN NUMERO (CONTATORE DEL NUMERO DI CLIENTI)
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {

						Registrazione application1 = new Registrazione();


					}
				}
				);

		accedi.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {

						Accedi application1 = new Accedi(1);
					}
				}
				);


		telefono.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {

						Telefono application1 = new Telefono();

					}
				}
				);


		logout.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {


						Libreria.logout();
						JOptionPane.showMessageDialog( Display.this, "Logout Effettato", "", JOptionPane.PLAIN_MESSAGE );
						logout.setVisible(false);
						accedi.setVisible(true);
						registrazione.setVisible(true);
						libri1.setText(" Libri al prossimo sconto: ");
						iconaLucchetto.setVisible(false);
						iconaLucchetto.setText("");
						libri1.setIcon(null);

					}
				}
				);

		// INSERIMENTO TITOLO PER ACQUISTO E PRENOTAZIONE


		titolo2.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {

						if ( acquista == true ) {          // SE IL CLIENTE DEVE ACQUISTARE

							int esito = Libreria.cercaLibro(titolo2.getText());
							if (esito == 0) {
								disponibile.setText(" Disponibile?   SI");
								prezzo.setText(" Prezzo :     "  + libroincorso.prezzo );
								fine = true;
								if (clienteincorso != null && clienteincorso.bonus == 1) {
									String b = Integer.toString(Display.clienteincorso.bonus);
									libri1.setText(" Libri al prossimo sconto:   " + b + "           -->  Prezzo scontato:  " + libroincorso.prezzo*0.9);
								}

							}
							else {
								if (esito == 1) {
									disponibile.setText(" Disponibile?   NO");
									JOptionPane.showMessageDialog( Display.this, "Non vi sono copie disponibili al momento. Richiesta di rifornimento gia' inviata.", "", JOptionPane.PLAIN_MESSAGE );
									titolo2.setText("");
									disponibile.setText(" Disponibile?");
								}
								else {
									disponibile.setText(" Disponibile?   NO"); 
									JOptionPane.showMessageDialog( Display.this, "Il libro non e' presente in libreria. ", " Libro non presente", JOptionPane.PLAIN_MESSAGE );
									titolo2.setText("");
									disponibile.setText(" Disponibile?");
								}
							}

						}

						else {                            // IL CLIENTE DEVE PRENOTARE
							int esito = Libreria.prestaLibro(titolo2.getText());
							if (esito == 0) {
								disponibile.setText(" Disponibile?   SI");
								fine = true;
							}
							else {
								if (esito == 1) {
									disponibile.setText(" Disponibile?   NO");
									int nc = usatoincorso.possiedeLibro;
									String s = Integer.toString(nc);
									JOptionPane.showMessageDialog( Display.this, "Il libro e' gia' in prestito al Cliente " + s, "", JOptionPane.PLAIN_MESSAGE );
									titolo2.setText("");
									disponibile.setText(" Disponibile?");
									usatoincorso = null;
								}
								else {
									disponibile.setText(" Disponibile?   NO");
									JOptionPane.showMessageDialog( Display.this, "Il libro non e' presente tra i libri usati.", "", JOptionPane.PLAIN_MESSAGE );
									titolo2.setText("");
									disponibile.setText(" Disponibile?");
								}

							}

						}

					}
				}
				);

		// JBUTTON CONFERMA

		conferma.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {

						if (fine == true) {

							if (acquista == true) {
								JOptionPane.showMessageDialog( Display.this, "Operazione Confermata!", "", JOptionPane.PLAIN_MESSAGE );
								libroincorso.numCopie--;
								numLibriVenduti++;
								if (clienteincorso == null) 
									ricavato += libroincorso.prezzo;
								if (clienteincorso != null) {          // SE A EFFETTUARE L'ACQUISTO E' STATO UN CLIENTE REGISTRATO..
									boolean bonusModified = false;
									Libreria.clienti[clienteincorso.numTessera].bonus--;
									String b = Integer.toString(Libreria.clienti[clienteincorso.numTessera].bonus);
									if (Libreria.clienti[clienteincorso.numTessera].bonus > 1) {
										libri1.setText(" Libri al prossimo sconto:    " + b);
										ricavato += libroincorso.prezzo;
									}
									else {                                                             // DECIMO ACQUISTO --> SCONTO 10%
										if (Libreria.clienti[clienteincorso.numTessera].bonus == 1) {
											Display.libri1.setText(" Libri al prossimo sconto:   " + b + " ");
											Display.libri1.setIcon(Display.cartello);
											ricavato += libroincorso.prezzo;
										}
										else {
											Display.libri1.setIcon(null);
											Libreria.clienti[clienteincorso.numTessera].bonus = 10;
											bonusModified = true;
											String p = Double.toString(libroincorso.prezzo * 0.9);
											JOptionPane.showMessageDialog( Display.this, "Il Cliente ha diritto al 10% di sconto su questo acquisto! Il prezzo e' dunque di " + p + " €", "", JOptionPane.PLAIN_MESSAGE );
											libri1.setText(" Libri al prossimo sconto:    " + "10");
											ricavato += libroincorso.prezzo*0.9;
										}
									}
									// AGGIORNO IL BONUS SUL FILE DI TESTO

									if (bonusModified == false) {   
										try {
											Scrittura.modificaFileClienti(Libreria.clienti[clienteincorso.numTessera].numTessera, Libreria.clienti[clienteincorso.numTessera].bonus + 1, Libreria.clienti[clienteincorso.numTessera].numTel, Libreria.clienti[clienteincorso.numTessera].bonus);
										} catch (FileNotFoundException e) {
										} catch (IOException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}
									else {
										try {
											Scrittura.modificaFileClienti(Libreria.clienti[clienteincorso.numTessera].numTessera, 1 , Libreria.clienti[clienteincorso.numTessera].numTel, 10);
										} catch (FileNotFoundException e) {
										} catch (IOException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
									}

								}
								if (libroincorso.numCopie == 0) {
									JOptionPane.showMessageDialog( Display.this, "Era l'ultima copia disponibile, inviata richiesta di rifornimento!", "", JOptionPane.PLAIN_MESSAGE );
									Libreria.rifornimento(libroincorso);             // CHIAMO IL RIFORNIMENTO
								}
								fine = false;
								disponibile.setText(" Disponibile?");
								titolo2.setText("");
								prezzo.setText(" Prezzo : "); 

								// MODIFICA DEL NUMERO DI COPIE NEL FILE DI TESTO

								String s = String.valueOf(libroincorso.prezzo);
								String s1 = String.valueOf(libroincorso.numCopie);
								String s2 = String.valueOf(libroincorso.numCopie + 1);
								String s3 = String.valueOf(libroincorso.anno);

								try {
									Scrittura.modificaFileNuovi(libroincorso.titolo, libroincorso.autore, s3, libroincorso.genere, s, s2, s1);
								} catch (IOException e) {}



								libroincorso = null;
							}
							else {             // STO DANDO IN PRESTITO
								if ( clienteincorso != null) {
									JOptionPane.showMessageDialog( Display.this, "Operazione Confermata, prestito avvenuto!", "Operazione effettuata", JOptionPane.PLAIN_MESSAGE );
									usatoincorso.inPrestito = 1; 
									usatoincorso.possiedeLibro = clienteincorso.numTessera;
									fine = false;

									// AGGIORNO IL FILE DI TESTO

									String s1 = String.valueOf(usatoincorso.possiedeLibro);
									try {
										Scrittura.modificaFileUsati(usatoincorso.titolo, usatoincorso.autore, usatoincorso.anno, usatoincorso.genere, "0", "-1", "1", s1);
									} catch (IOException e) {}


									usatoincorso = null;
									disponibile.setText(" Disponibile?");
									titolo2.setText("");
								}
								else {
									Accedi application1 = new Accedi(1);
									
								}
							}
						}

						else {
							JOptionPane.showMessageDialog( Display.this, "Nessuna operazione richiesta!", "Operazione non effettuata", JOptionPane.PLAIN_MESSAGE );
						}

					}
				}
				);

		setResizable( false );
		setSize( 830, 500 );
		setLocationRelativeTo(null);
		setVisible( true );




		// ALLA CHIUSURA

		this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				int delete = JOptionPane.showConfirmDialog(null, "Vuoi uscire dal programma? ","Attenzione !",JOptionPane.YES_NO_OPTION);	
				if(delete==0){
					if (numLibriVenduti == 0) {
						JOptionPane.showMessageDialog( Display.this, "Nessun libro venduto oggi!", "Resoconto di fine giornata", JOptionPane.PLAIN_MESSAGE );
					}
					else {
						JOptionPane.showMessageDialog( Display.this, "Numero di libri venduti:   " + numLibriVenduti + "\nRicavato totale:   " + ricavato + " Euro.", "Resoconto di fine giornata", JOptionPane.PLAIN_MESSAGE );
					}
					System.exit(0);
				}
			}
		});


	} // END CONSTRUCTOR


	// CLASSE PER GESTIRE EVENTI DEI RADIOBUTTON

	private class ItemHandler implements ActionListener {

		public void actionPerformed( ActionEvent event ) {

			if (event.getSource() == scelta[0]) {
				acquista = true;
				prezzo.setVisible(true);


			}

			if (event.getSource() == scelta[1]) {
				acquista = false;
				prezzo.setVisible(false);

			}
		}
	}


	//METHOD ADDCOMPONENT

	private void addComponent( Component component, int row, int column, int width, int height ) {

		constraints.gridx = column;
		constraints.gridy = row;

		constraints.gridwidth = width;
		constraints.gridheight = height;

		tabella.setConstraints( component, constraints );
		container.add( component ); 
	}

}   // END CLASS