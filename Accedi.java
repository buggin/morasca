import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class Accedi extends JFrame {

	private JTextField numTessera2;
	private JLabel numTessera1;
	private JButton accedi, annulla;
	private int richiesta;           // = UGUALE A 0 SE DEVO LANCIARE 'AGGIUNGI USATO'

	private Container container;
	private GridBagLayout tabella;
	private GridBagConstraints constraints;

	public Accedi (int _richiesta) {

		super ("Accedi");

		richiesta = _richiesta;

		container = getContentPane();
		tabella = new GridBagLayout ();
		container.setLayout(tabella);
		constraints = new GridBagConstraints();

		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 1000;
		constraints.weighty = 1000;

		numTessera1 = new JLabel("   Inserire il Numero di Tessera ");              // FISICAMENTE CORRISPONDE ALLA CONSEGNA DELLA TESSERA E ALLA LETTURA DELLA STESSA TRAMITE UN LETTORE DI TESSERE (LA TESSERA E' MAGNETICA)
		numTessera1.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		numTessera2 = new JTextField("");
		numTessera2.setEditable(true);
		numTessera2.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		accedi = new JButton("Accedi");
		accedi.setFont(new Font( "Serif", Font.PLAIN, 15 ) );

		annulla = new JButton("Annulla");
		annulla.setFont(new Font( "Serif", Font.PLAIN, 15 ) );

		// ADDCOMPONENT

		addComponent (numTessera1,       0, 0, 2, 1);
		addComponent (numTessera2,       0, 2, 2, 1);
		addComponent (annulla,           1, 2, 1, 1);
		addComponent (accedi,            1, 3, 1, 1);

		// EVENTI


		annulla.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						setVisible( false ); 
					}
				}
				);

		accedi.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {

						if (!(numTessera2.getText().equals(""))) {
							int s = Integer.parseInt(numTessera2.getText());
							Display.clienteincorso = Libreria.accedi(s);
							JOptionPane.showMessageDialog( Accedi.this, "Accesso effettuato!", "Operazione completata", JOptionPane.PLAIN_MESSAGE);
							setVisible( false );
							Display.iconaLucchetto.setVisible(true);
							Display.iconaLucchetto.setText("Cliente " + Display.clienteincorso.numTessera);
							Display.libri1.setVisible(true);
							String b = Integer.toString(Display.clienteincorso.bonus);
							if (Display.clienteincorso.bonus == 1) {
								Display.libri1.setText(" Libri al prossimo sconto:   " + b + " ");
								Display.libri1.setIcon(Display.cartello);
							}
							else {
								Display.libri1.setText(" Libri al prossimo sconto:   " + b);
							}
							Display.logout.setVisible(true);
							Display.accedi.setVisible(false);
							Display.registrazione.setVisible(false);

							if (richiesta == 0) {
								AggiungiUsato application1 = new AggiungiUsato();
							}
						}
						else {
							JOptionPane.showMessageDialog( Accedi.this, "Errore nella compilazione!", "Operazione non riuscita", JOptionPane.PLAIN_MESSAGE);
						}

					}
				}
				);

		setResizable( false );
		setSize( 700, 100 );
		setLocationRelativeTo(null);
		setVisible( true );

	}   // END CONSTRUCTOR

	//METODO ADDCOMPONENT

	private void addComponent( Component component, int row, int column, int width, int height ) {

		constraints.gridx = column;
		constraints.gridy = row;

		constraints.gridwidth = width;
		constraints.gridheight = height;

		tabella.setConstraints( component, constraints );
		container.add( component ); 
	}


} // END CLASS