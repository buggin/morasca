import java.awt.*;
import java.awt.event.*;
import java.io.FileNotFoundException;

import javax.swing.*;

public class AggiungiNuovo extends JFrame {

	private JTextField titolo1, autore1, anno1, genere1, numCopie1, prezzo1;
	private JLabel titolo, autore, anno, genere, numCopie, prezzo;
	private JButton conferma, annulla;

	private Container container;
	private GridBagLayout tabella;
	private GridBagConstraints constraints;

	public AggiungiNuovo () {

		super ("Aggiungi Nuovo Libro");
		getContentPane().setForeground(Color.BLACK);

		container = getContentPane();
		tabella = new GridBagLayout ();
		container.setLayout(tabella);
		constraints = new GridBagConstraints();

		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 1000;
		constraints.weighty = 1000;

		titolo = new JLabel("   Inserire Titolo ");
		titolo.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		titolo1 = new JTextField("");
		titolo1.setEditable(true);
		titolo1.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		autore = new JLabel("   Inserire Autore ");
		autore.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		autore1 = new JTextField("");
		autore1.setEditable(true);
		autore1.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		anno = new JLabel("   Inserire Anno ");
		anno.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		anno1 = new JTextField("");
		anno1.setEditable(true);
		anno1.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		genere = new JLabel("   Inserire Genere ");
		genere.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		genere1 = new JTextField("");
		genere1.setEditable(true);
		genere1.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		numCopie = new JLabel("   Inserire il Numero di Copie ");
		numCopie.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		numCopie1 = new JTextField("");
		numCopie1.setEditable(true);
		numCopie1.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		prezzo = new JLabel("   Inserire Prezzo ");
		prezzo.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		prezzo1 = new JTextField("");
		prezzo1.setEditable(true);
		prezzo1.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		conferma = new JButton("Conferma");
		conferma.setFont(new Font( "Serif", Font.PLAIN, 15 ) );

		annulla = new JButton("Annulla");
		annulla.setFont(new Font( "Serif", Font.PLAIN, 15 ) );

		// ADDCOMPONENT
		addComponent (titolo,      0, 0, 1, 1);
		addComponent (titolo1,     0, 1, 5, 1);
		addComponent (autore,      1, 0, 1, 1);
		addComponent (autore1,     1, 1, 5, 1);
		addComponent (anno,        2, 0, 1, 1);
		addComponent (anno1,       2, 1, 5, 1);
		addComponent (genere,      3, 0, 1, 1);
		addComponent (genere1,     3, 1, 5, 1);
		addComponent (numCopie,    4, 0, 1, 1);
		addComponent (numCopie1,   4, 1, 5, 1);
		addComponent (prezzo,      5, 0, 1, 1);
		addComponent (prezzo1,     5, 1, 5, 1);
		addComponent (annulla,     6, 3, 1, 1);
		addComponent (conferma,    6, 4, 1, 1);





		// EVENTI

		annulla.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						setVisible (false );
					}
				});
		conferma.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						boolean can = true;
						if ( titolo1.getText().equals("") | autore1.getText().equals("") | anno1.getText().equals("") | genere1.getText().equals("") | prezzo1.getText().equals("") | numCopie1.getText().equals("")) {
							JOptionPane.showMessageDialog( AggiungiNuovo.this, "Errore nella compilazione!", "Operazione non riuscita", JOptionPane.PLAIN_MESSAGE); }
						else {
							try {
								int n = Integer.parseInt(anno1.getText());
								int n1 = Integer.parseInt(prezzo1.getText());
								int n2 = Integer.parseInt(numCopie1.getText());
							}
							catch (java.lang.NumberFormatException e) {
								JOptionPane.showMessageDialog( AggiungiNuovo.this, "Errore nella compilazione!", "", JOptionPane.PLAIN_MESSAGE);
								can = false;
							}
							if (can) {
								int nc = Integer.parseInt(numCopie1.getText());
								double p = Double.parseDouble(prezzo1.getText()); 
								Nuovo n = new Nuovo (titolo1.getText(), autore1.getText(), anno1.getText(), genere1.getText(), nc , p);
								try {
									Libreria.addLibro(n);
								} catch (FileNotFoundException e) {
								}
								JOptionPane.showMessageDialog( AggiungiNuovo.this, "Operazione Completata, libro aggiunto!", "Operazione completata", JOptionPane.PLAIN_MESSAGE);

								setVisible (false );
							}}
					
	}});

		setResizable( false );
		setSize( 800, 350 );
		setLocationRelativeTo(null);
		setVisible( true );
	}   // END CONSTRUCTOR

	//METODO ADDCOMPONENT
	private void addComponent( Component component, int row, int column, int width, int height ) {
		constraints.gridx = column;
		constraints.gridy = row;
		constraints.gridwidth = width;
		constraints.gridheight = height;
		tabella.setConstraints( component, constraints );
		container.add( component ); }
} // END CLASS