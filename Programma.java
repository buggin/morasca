import java.io.*;
import java.nio.file.*;

public class Programma {

	//CAMPI DEL PROGRAMMA
	//FileSystems.getDefault().getPath();
	protected static String pathClienti = "clienti.txt";// N.B. INSERIRE UN PATH ADATTO AL COMPUTER IN USO
	protected static File fileClienti;

	protected static String pathNuovi = "nuovi.txt";  // N.B. INSERIRE UN PATH ADATTO AL COMPUTER IN USO
	protected static File fileNuovi;

	protected static String pathVecchi = "usati.txt";  // N.B. INSERIRE UN PATH ADATTO AL COMPUTER IN USO
	protected static File fileVecchi;


	public static void main( String args[] ) throws IOException {
		fileClienti = daStringaAFile(pathClienti,"");
		fileNuovi = daStringaAFile(pathNuovi, "");
		fileVecchi = daStringaAFile(pathVecchi, "");
		
		Display application = new Display();
		

		// FILE DI TESTO
		controlloFile();

		// CREAZIONE DEI CLIENTI ESISTENTI (DA FILE DI TESTO)
		creaClienti();

		// CREAZIONE DEI LIBRI NUOVI ESISTENTI (DA FILE DI TESTO)
		creaNuovi();

		// CREAZIONE DEI LIBRI USATI ESISTENTI (DA FILE DI TESTO)
		creaUsati();


	}    // END MAIN

	/**
	 * Crea i libri usati salvati sul file di testo usati.txt
	 * @throws IOException
	 */
	private static void creaUsati() throws IOException {
		FileReader fr = new FileReader(fileVecchi);
		BufferedReader br = new BufferedReader(fr);

		char[] in = new char[100];
		int size = br.read(in);

		// CONTO I ; CHE CI SONO NEL FILE DI TESTO, PER AVERE IL NUMERO DI LIBRI

		int contatore = 0;
		for (int i11 = 0; i11 < size; i11++) {
			if (in[i11] == ';') 
				contatore++;
		}

		Libreria.numUsati = contatore/6;    // OGNI LIBRO HA INFATTI 6 PUNTI E VIRGOLA


		String titolo = "";
		String autore = "";
		String anno = "";
		String genere = "";
		String inPrestito = "";
		String prestitoCliente = "";

		int i = 0;

		if ( i < size) {                                // SE IL FILE E' VUOTO (SIZE = -1) NON FACCIO NULLA


			int cont = 0;

			while (cont < Libreria.numUsati) {

				while (in[i] != ';') {
					String s = String.valueOf(in[i]);
					titolo = titolo + s;
					i++;
				}

				i++;


				while (in[i] != ';') {
					String s1 = String.valueOf(in[i]);
					autore = autore + s1;
					i++;
				}

				i++;

				while (in[i] != ';') {
					String s2 = String.valueOf(in[i]);
					anno = anno + s2;
					i++;
				}

				i++;

				while (in[i] != ';') {
					String s3 = String.valueOf(in[i]);
					genere = genere + s3;
					i++;
				}

				i++;


				while (in[i] != ';') {
					String s4 = String.valueOf(in[i]);
					inPrestito = inPrestito + s4;
					i++;
				}

				i++;


				while (in[i] != ';') {
					String s5 = String.valueOf(in[i]);
					prestitoCliente = prestitoCliente + s5;
					i++;
				}

				i = i + 2;

				int inPrestitoInt = Integer.parseInt(inPrestito);
				int clienteInt = Integer.parseInt(prestitoCliente);


				Usato usato = new Usato (titolo, autore, anno, genere);
				usato.inPrestito = inPrestitoInt;
				usato.possiedeLibro = clienteInt;


				Libreria.usati[cont] = usato;


				cont++;

				titolo = "";
				autore = "";
				anno = "";
				genere = "";
				inPrestito = "";
				prestitoCliente = "";

			}

		}

	}

	/**
	 * Crea i libri nuovi salvati sul file di testo nuovi.txt
	 * @throws IOException
	 */
	private static void creaNuovi() throws IOException {
		FileReader fr = new FileReader(fileNuovi);
		BufferedReader br = new BufferedReader(fr);

		char[] in = new char[100];
		int size = br.read(in);

		// CONTO I ; CHE CI SONO NEL FILE DI TESTO, PER AVERE IL NUMERO DI LIBRI

		int cont = 0;
		for (int i = 0; i < size; i++) {
			if (in[i] == ';') 
				cont++;
		}

		Libreria.numLibri = cont/6;    // OGNI LIBRO HA INFATTI 6 PUNTI E VIRGOLA

		String titolo = "";
		String autore = "";
		String anno = "";
		String genere = "";
		String prezzo = "";
		String numeroCopie = "";


		int i = 0;

		if ( i < size) {                                // SE IL FILE E' VUOTO (SIZE = -1) NON FACCIO NULLA
			int contatore = 0;
			while (contatore < Libreria.numLibri) {
				while (in[i] != ';') {
					String s = String.valueOf(in[i]);
					titolo = titolo + s;
					i++;
				}

				i++;


				while (in[i] != ';') {
					String s1 = String.valueOf(in[i]);
					autore = autore + s1;
					i++;
				}


				i++;

				while (in[i] != ';') {
					String s2 = String.valueOf(in[i]);
					anno = anno + s2;
					i++;
				}


				i++;

				while (in[i] != ';') {
					String s3 = String.valueOf(in[i]);
					genere = genere + s3;
					i++;
				}

				i++;


				while (in[i] != ';') {
					String s4 = String.valueOf(in[i]);
					prezzo = prezzo + s4;
					i++;
				}

				i++;


				while (in[i] != ';') {
					String s5 = String.valueOf(in[i]);
					numeroCopie = numeroCopie + s5;
					i++;
				}

				i = i + 2;

				int numeroCopieInt = Integer.parseInt(numeroCopie);
				double prezzoInt = Double.parseDouble(prezzo);


				Nuovo nuovo = new Nuovo (titolo, autore, anno, genere, numeroCopieInt, prezzoInt);


				Libreria.listalibri[contatore] = nuovo;


				contatore++;

				titolo = "";
				autore = "";
				anno = "";
				genere = "";
				prezzo = "";
				numeroCopie = "";

			}

		}

	}

	/**
	 * Crea i clienti salvati sul file di testo clienti.txt
	 * @throws IOException
	 */
	private static void creaClienti() throws IOException {
		FileReader fr = new FileReader(fileClienti);
		BufferedReader br = new BufferedReader(fr);

		char[] in = new char[1000];
		int size = br.read(in);

		// CONTO I ; CHE CI SONO NEL FILE DI TESTO, PER AVERE IL NUMERO DI CLIENTI

		int contat = 0;
		for (int i = 0; i < size; i++) {
			if (in[i] == ';') 
				contat++;
		}

		br.close();

		Libreria.numClienti = contat/3;

		String numeroTessera = "";
		String bonus = "";
		String telefono = "";



		int i = 0;

		if ( i < size) {                                // SE IL FILE E' VUOTO (SIZE = -1) NON FACCIO NULLA


			int contatore = 0;

			while (contatore < Libreria.numClienti) {



				while (in[i] != ';') {
					String s = String.valueOf(in[i]);
					numeroTessera = numeroTessera + s;
					i++;
				}	

				i++;

				while (in[i] != ';') {
					String s1 = String.valueOf(in[i]);
					bonus = bonus + s1;
					i++;
				}

				i++;

				while (in[i] != ';') {
					String s2 = String.valueOf(in[i]);
					telefono = telefono + s2;
					i++;
				}

				i++;


				i = i + 1;

				int tessera = Integer.parseInt(numeroTessera);
				int bonusInt = Integer.parseInt(bonus);

				Cliente nuovo = new Cliente (tessera, telefono);
				nuovo.bonus = bonusInt;

				Libreria.clienti[contatore] = nuovo;

				contatore++;

				numeroTessera = "";
				bonus = "";
				telefono = "";

			}

		}

	}


	/**
	 * Crea i file di testo (relativi a clienti, libri nuovi e libri usati) se non esisitono gia
	 * @throws IOException
	 */
	private static void controlloFile() throws IOException {
		if (!fileClienti.exists())                                                       
			if (!fileClienti.createNewFile())
				System.err.println("Il file " + pathClienti + " non puo essere creato");

		if (!fileNuovi.exists())                     
			if (!fileNuovi.createNewFile()) 
				System.err.println("Il file " + pathNuovi + " non puo essere creato");

		if (!fileVecchi.exists())              
			if (!fileVecchi.createNewFile()) 
				System.err.println("Il file " + pathVecchi + " non puo essere creato");

	}

	private static File daStringaAFile(String titolo, String posizione) {
		return new File(FileSystems.getDefault().getPath(posizione,titolo).toString());
	}

}