import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Restituzione extends JFrame {

	private JTextField titolo1;
	private JLabel titolo;
	private JButton conferma, annulla;

	private Container container;
	private GridBagLayout tabella;
	private GridBagConstraints constraints;

	public Restituzione () {

		super ("Restituzione Libro");

		container = getContentPane();
		tabella = new GridBagLayout ();
		container.setLayout(tabella);
		constraints = new GridBagConstraints();

		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 1000;
		constraints.weighty = 1000;

		titolo = new JLabel("   Inserire Titolo ");
		titolo.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		titolo1 = new JTextField("");
		titolo1.setEditable(true);
		titolo1.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		conferma = new JButton("Conferma");
		conferma.setFont(new Font( "Serif", Font.PLAIN, 15 ) );

		annulla = new JButton("Annulla");
		annulla.setFont(new Font( "Serif", Font.PLAIN, 15 ) );
		
		// ADDCOMPONENT
		addComponent (titolo,       0, 0, 2, 1);
		addComponent (titolo1,      0, 2, 2, 1);
		addComponent (annulla,      1, 2, 1, 1);
		addComponent (conferma,     1, 3, 1, 1);

		// EVENTI
		
		annulla.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						setVisible (false ); 
					}
				});

		conferma.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						if (!( titolo1.getText().equals(""))) {
							int esito = Libreria.restituzioneLibro(titolo1.getText());
							if (esito == 0) {
								JOptionPane.showMessageDialog( Restituzione.this, "Operazione Completata!", "Operazione completata", JOptionPane.PLAIN_MESSAGE);
								setVisible (false ); 
							}
							else {
								if (esito == 1) {
									JOptionPane.showMessageDialog( Restituzione.this, "Il libro non era in prestito!", "Operazione non effettuata", JOptionPane.PLAIN_MESSAGE);
									titolo1.setText(""); 
								}
								else {
									JOptionPane.showMessageDialog( Restituzione.this, "Il libro non esiste tra  libri usati!", "Operazione non riuscita", JOptionPane.PLAIN_MESSAGE);
									titolo1.setText(""); 
								}
							}
						}
						else {
							JOptionPane.showMessageDialog( Restituzione.this, "Errore nella compilazione!", "Operazione non riuscita", JOptionPane.PLAIN_MESSAGE);
						}
					}});
		setResizable( false );
		setSize( 800, 100 );
		setLocationRelativeTo(null);
		setVisible( true );
	}   // END CONSTRUCTOR
	
	
	//METODO ADDCOMPONENT
	private void addComponent( Component component, int row, int column, int width, int height ) {

		constraints.gridx = column;
		constraints.gridy = row;
		constraints.gridwidth = width;
		constraints.gridheight = height;
		tabella.setConstraints( component, constraints );
		container.add( component ); }
} // END CLASS