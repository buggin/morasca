import java.awt.*;
import java.awt.event.*;

import javax.swing.*;

public class Rifornimento extends JFrame {

	private JTextField titolo1, numCopie1;
	private JLabel titolo, numCopie;
	private JButton conferma, annulla;

	private Container container;
	private GridBagLayout tabella;
	private GridBagConstraints constraints;

	public Rifornimento () {

		super ("Rifornimento arrivato");

		container = getContentPane();
		tabella = new GridBagLayout ();
		container.setLayout(tabella);
		constraints = new GridBagConstraints();

		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 1000;
		constraints.weighty = 1000;

		titolo = new JLabel("   Inserire Titolo ");
		titolo.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		titolo1 = new JTextField("");
		titolo1.setEditable(true);
		titolo1.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		numCopie = new JLabel("   Inserire Numero di Copie ");
		numCopie.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		numCopie1 = new JTextField("");
		numCopie1.setEditable(true);
		numCopie1.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		conferma = new JButton("Conferma");
		conferma.setFont(new Font( "Serif", Font.PLAIN, 15 ) );

		annulla = new JButton("Annulla");
		annulla.setFont(new Font( "Serif", Font.PLAIN, 15 ) );

		// ADDCOMPONENT
		addComponent (titolo,       0, 0, 2, 1);
		addComponent (titolo1,      0, 2, 2, 1);
		addComponent (numCopie,     1, 0, 2, 1);
		addComponent (numCopie1,    1, 2, 2, 1);
		addComponent (annulla,      2, 2, 1, 1);
		addComponent (conferma,     2, 3, 1, 1);

		// EVENTI

		annulla.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						setVisible (false ); 
					}
				});

		conferma.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						boolean can = true;
						if ( titolo1.getText().equals("") | numCopie1.getText().equals("")) {
							JOptionPane.showMessageDialog( Rifornimento.this, "Errore nella compilazione!", "Operazione non riuscita", JOptionPane.PLAIN_MESSAGE);
						}
						else {
							try {
								int n = Integer.parseInt(numCopie1.getText());
							}
							catch (java.lang.NumberFormatException e) {
								JOptionPane.showMessageDialog( Rifornimento.this, "Errore nella compilazione!", "", JOptionPane.PLAIN_MESSAGE);
								can = false;
							}
							if (can) {
								int nc = Integer.parseInt(numCopie1.getText());
								int esito = Libreria.rifornimentoArrivato(titolo1.getText(), nc);
								if (esito == 0) {
									JOptionPane.showMessageDialog( Rifornimento.this, "Operazione Completata!", "Operazione completata", JOptionPane.PLAIN_MESSAGE);
									setVisible (false ); 
								}
								else {
									JOptionPane.showMessageDialog( Rifornimento.this, "Il libro non esiste!", "Operazione non riuscita", JOptionPane.PLAIN_MESSAGE);
									titolo1.setText(""); 
                                    numCopie1.setText("");
								}}
						}
					}});
		setResizable( false );
		setSize( 825, 150 );
		setLocationRelativeTo(null);
		setVisible( true );
	}   // END CONSTRUCTOR

	//METODO ADDCOMPONENT
	private void addComponent( Component component, int row, int column, int width, int height ) {
		constraints.gridx = column;
		constraints.gridy = row;

		constraints.gridwidth = width;
		constraints.gridheight = height;

		tabella.setConstraints( component, constraints );
		container.add( component ); }
} // END CLASS