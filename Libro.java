public abstract class Libro {             
	
	protected String titolo, autore, anno, genere;
	
	public Libro ( String t, String a, String an, String g) {
		
		titolo = t;
		autore = a;
		anno = an;
		genere = g;
		
	}
	
	public boolean equals ( Libro l) {
		
		if (this.titolo.equals(l.titolo) ) {
			return true; }
			
		else {
			return false; }
				
	}
	

}