public class Usato extends Libro {
	
	protected int inPrestito = 0;           // 0 --> NON IN PRESTITO, 1 --> IN PRESTITO
    protected int possiedeLibro = -1;       // NUMERO DI TESSERA DEL CLIENTE CHE HA IL LIBRO IN PRESTITO
	

	public Usato (String t, String a, String an, String g) {
		super (t, a, an, g);
	}
}