import java.awt.*;
import java.awt.event.*;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.*;

public class AggiungiUsato extends JFrame {

	private JTextField titolo1, autore1, anno1, genere1;
	private JLabel titolo, autore, anno, genere;
	private JButton conferma, annulla;

	private Container container;
	private GridBagLayout tabella;
	private GridBagConstraints constraints;

	public AggiungiUsato () {

		super ("Aggiungi Libro Usato");

		container = getContentPane();
		tabella = new GridBagLayout ();
		container.setLayout(tabella);
		constraints = new GridBagConstraints();
		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 1000;
		constraints.weighty = 1000;

		titolo = new JLabel("   Inserire Titolo ");
		titolo.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		titolo1 = new JTextField("");
		titolo1.setEditable(true);
		titolo1.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		autore = new JLabel("   Inserire Autore ");
		autore.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		autore1 = new JTextField("");
		autore1.setEditable(true);
		autore1.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		anno = new JLabel("   Inserire Anno ");
		anno.setFont(new Font( "Serif", Font.PLAIN, 20 ) );


		anno1 = new JTextField("");
		anno1.setEditable(true);
		anno1.setFont(new Font( "Serif", Font.PLAIN, 20 ) );


		genere = new JLabel("   Inserire Genere ");
		genere.setFont(new Font( "Serif", Font.PLAIN, 20 ) );


		genere1 = new JTextField("");
		genere1.setEditable(true);
		genere1.setFont(new Font( "Serif", Font.PLAIN, 20 ) );


		conferma = new JButton("Conferma");
		conferma.setFont(new Font( "Serif", Font.PLAIN, 15 ) );

		annulla = new JButton("Annulla");
		annulla.setFont(new Font( "Serif", Font.PLAIN, 15 ) );

		// ADDCOMPONENT
		addComponent (titolo,      0, 0, 2, 1);
		addComponent (titolo1,     0, 2, 2, 1);
		addComponent (autore,      1, 0, 2, 1);
		addComponent (autore1,     1, 2, 2, 1);
		addComponent (anno,        2, 0, 2, 1);
		addComponent (anno1,       2, 2, 2, 1);
		addComponent (genere,      3, 0, 2, 1);
		addComponent (genere1,     3, 2, 2, 1);
		addComponent (annulla,     4, 2, 1, 1);
		addComponent (conferma,    4, 3, 1, 1);

		// EVENTI

		annulla.addActionListener(
				new ActionListener() {

					public void actionPerformed( ActionEvent event ) {
						setVisible (false );
					}
				});



		conferma.addActionListener(
				new ActionListener() {

					public void actionPerformed( ActionEvent event ) {
						boolean can = true;
						if ( titolo1.getText().equals("") | autore1.getText().equals("") | anno1.getText().equals("") | genere1.getText().equals("")) {
							JOptionPane.showMessageDialog( AggiungiUsato.this, "Errore nella compilazione!", "Operazione non riuscita", JOptionPane.PLAIN_MESSAGE); }
						else {
							try {
								int n = Integer.parseInt(anno1.getText());
							}
							catch (java.lang.NumberFormatException e) {
								JOptionPane.showMessageDialog( AggiungiUsato.this, "Errore nella compilazione!", "", JOptionPane.PLAIN_MESSAGE);
								can = false;
							}
							if (can) {
								Usato n = new Usato (titolo1.getText(), autore1.getText(), anno1.getText(), genere1.getText());
								int esito = 0;
								try {
									esito = Libreria.addUsato(n);
								} catch (FileNotFoundException e) {
								}
								if (esito == 0) {
									JOptionPane.showMessageDialog( AggiungiUsato.this, "Operazione Completata, libro aggiunto!", "Operazione completata", JOptionPane.PLAIN_MESSAGE);
									int bonusVecchio = Libreria.clienti[Display.clienteincorso.numTessera].bonus;
									Libreria.clienti[Display.clienteincorso.numTessera].bonus -= 2;
									if (Libreria.clienti[Display.clienteincorso.numTessera].bonus < 1) 
										Libreria.clienti[Display.clienteincorso.numTessera].bonus = 1;
									
									if (Libreria.clienti[Display.clienteincorso.numTessera].bonus == 1) {
										String b = Integer.toString(Libreria.clienti[Display.clienteincorso.numTessera].bonus);
										Display.libri1.setText(" Libri al prossimo sconto:   " + b + " ");
										Display.libri1.setIcon(Display.cartello);
									}


									// RISCRITTURA FILE DI TESTO

									try {
										Scrittura.modificaFileClienti(Display.clienteincorso.numTessera, bonusVecchio, Display.clienteincorso.numTel, Libreria.clienti[Display.clienteincorso.numTessera].bonus );
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}

									String s = Integer.toString(Libreria.clienti[Display.clienteincorso.numTessera].bonus);
									Display.libri1.setText(" Libri al prossimo sconto:   " + s);
									setVisible (false );
								}

								else {
									JOptionPane.showMessageDialog( AggiungiUsato.this, "Ho gia questo libro tra i libro usati, non verra aggiunto!", "Operazione non effettuata", JOptionPane.PLAIN_MESSAGE );
									setVisible (false );
								}
							}
						}

					}});


		setResizable( false );
		setSize( 800, 250 );
		setLocationRelativeTo(null);
		setVisible( true );

	}   // END CONSTRUCTOR

	//METODO ADDCOMPONENT
	private void addComponent( Component component, int row, int column, int width, int height ) {
		constraints.gridx = column;
		constraints.gridy = row;
		constraints.gridwidth = width;
		constraints.gridheight = height;
		tabella.setConstraints( component, constraints );
		container.add( component ); 
	}
} // END CLASS