import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;


public class Scrittura {

	protected static FileInputStream fstream = null;  
	protected static DataInputStream in = null;
	protected static BufferedWriter out = null;
	protected static BufferedReader br = null;
	static String strLine;
	static StringBuilder fileContent ;

	// METODI PER IL FILE CLIENTI.TXT

	/**
	 * Scrive sul file di testo clienti.txt il cliente tramite i dati passati come parametro
	 * @param numeroTessera Numero di tessera del cliente
	 * @param bonus Bonus acqusiti relativi al cliente
	 * @param numeroTelefono Numero di telefono del cliente
	 * @throws IOException
	 */
	public static void iscriviCliente(int numeroTessera, int bonus, String numeroTelefono) throws IOException {

		FileOutputStream fos=new FileOutputStream(Programma.fileClienti, true);
		PrintStream ps=new PrintStream(fos);

		String s1 = String.valueOf(numeroTessera);
		String s2 = String.valueOf(bonus);
		ps.append(s1 + ";");
		ps.append(s2 + ";");
		ps.append(numeroTelefono + ";\n");
		ps.close();

	}

	/**
	 * Modifica il bonus di un cliente (i cui dati sono passati come parametro) sul file clienti.txt 
	 * @param numeroTessera Numero di tessera del cliente
	 * @param bonusVecchio Bonus del cliente prima della modifica
	 * @param numeroTelefono Numero di telefono del cliente
	 * @param bonusNuovo Bonus del cliente in seguito alla modifica
	 * @throws IOException
	 */
	public static void modificaFileClienti(int numeroTessera, int bonusVecchio, String numeroTelefono, int bonusNuovo) throws IOException {

		try {
			BufferedReader br = accediLettura("clienti.txt");
			String strLine;
			StringBuilder fileContent = new StringBuilder();

			String s = String.valueOf(numeroTessera);
			String s1 = String.valueOf(bonusVecchio);
			String s2 = String.valueOf(bonusNuovo);

			// Leggo il file riga per riga
			while ((strLine = br.readLine()) != null) {
				if(strLine.equals(s + ";" + s1 + ";" + numeroTelefono + ";")){
					// se la riga e uguale a quella ricercata
					fileContent.append(s + ";" + s2 + ";" + numeroTelefono + ";" + System.getProperty("line.separator"));
				} else {
					// ... altrimenti la trascrivo cosi com'e
					fileContent.append(strLine);
					fileContent.append(System.getProperty("line.separator"));
				}
			}
			// Sovrascrivo il file con il nuovo contenuto (aggiornato)
			out = sovrascrivo("clienti.txt",fileContent);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			// chiusura dell'output e dell'input
			chiudoStream();
		}

	}


	// METODI PER IL FILE NUOVI.TXT

	/**
	 * Scrive sul file di testo nuovi.txt il libro nuovo tramite i dati passati come parametro
	 * @param titolo Titolo del libro nuovo
	 * @param autore Autore del libro
	 * @param anno Anno di uscita del libro
	 * @param genere Genere del libro
	 * @param prezzo Prezzo del libro
	 * @param numeroCopie Numero copie in libreria 
	 * @throws FileNotFoundException
	 */
	public static void aggiungiNuovo(String titolo, String autore, String anno, String genere, String prezzo, String numeroCopie) throws FileNotFoundException {

		FileOutputStream fos=new FileOutputStream(Programma.fileNuovi, true);
		PrintStream ps=new PrintStream(fos);

		ps.append(titolo + ";");
		ps.append(autore + ";");
		ps.append(anno + ";");
		ps.append(genere + ";");
		ps.append(prezzo + ";");
		ps.append(numeroCopie + ";\n");


		ps.close();


	}

	/**
	 * Modifica il numero di copie relative a un libro (i cui dati sono passati come parametro) sul file nuovi.txt
	 * @param titolo Titolo del libro nuovo
	 * @param autore Autore del libro
	 * @param anno Anno di uscita del libro
	 * @param genere Genere del libro
	 * @param prezzo Prezzo del libro
	 * @param numeroCopie Numero copie in libreria prima della modifica
	 * @param numeroCopieNuovo Numero copie in libreria dopo la modifica
	 * @throws IOException
	 */
	public static void modificaFileNuovi(String titolo, String autore, String anno, String genere, String prezzo, String numeroCopie, String numeroCopieNuovo) throws IOException {
		try {
			br = accediLettura("nuovi.txt");
			fileContent = new StringBuilder();

			// Leggo il file riga per riga
			while ((strLine = br.readLine()) != null) {
				if(strLine.equals(titolo + ";" + autore + ";" + anno + ";" + genere + ";" + prezzo + ";" + numeroCopie + ";")){
					// se la riga e uguale a quella ricercata
					fileContent.append(titolo + ";" + autore + ";" + anno + ";" + genere + ";" + prezzo + ";" + numeroCopieNuovo + ";" +System.getProperty("line.separator"));
				} else {
					// ... altrimenti la trascrivo cosi com'e
					fileContent.append(strLine);
					fileContent.append(System.getProperty("line.separator"));
				}
			}
			// Sovrascrivo il file con il nuovo contenuto (aggiornato)
			sovrascrivo("nuovi.txt", fileContent);
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			// chiusura dell'output e dell'input
			chiudoStream();
		}

	}

	// METODI PER IL FILE USATI.TXT


	/**
	 * Scrive sul file di testo usati.txt il libro usato i cui dati sono passati come parametro
	 * @param titolo Titolo del libro usato
	 * @param autore Autore del libro usato
	 * @param anno Anno di uscita del libro usato
	 * @param genere Genere del libro usato
	 * @param inPrestito Indica se il libro e in prestito o no
	 * @param prestitoCliente Contiene le informazioni del cliente che ha preso in prestito
	 * @throws FileNotFoundException
	 */
	public static void aggiungiUsato(String titolo, String autore, String anno, String genere, String inPrestito, String prestitoCliente) throws FileNotFoundException {

		FileOutputStream fos=new FileOutputStream(Programma.fileVecchi, true);
		PrintStream ps=new PrintStream(fos);

		ps.append(titolo + ";");
		ps.append(autore + ";");
		ps.append(anno + ";");
		ps.append(genere + ";");
		ps.append(inPrestito + ";");
		ps.append(prestitoCliente + ";\n");


		ps.close();


	}


	/**
	 * Modifica il file usati.txt in seguito ad un prestito
	 * @param titolo Titolo del libro
	 * @param autore Autore del libro
	 * @param anno Anno di uscita del libro
	 * @param genere Genere del libro
	 * @param inPrestito Indica se il libro e in prestito o no prima della modifica
	 * @param nClienteLibro Contiene le informazioni relative al cliente che ha in prestito il libro prima della modifica
	 * @param inPrestitoNuovo Indica se il libro e in prestito o no dopo la modifica
	 * @param possiedeLibroNuovo Contiene le informazioni relative al cliente che ha in prestito il libro dopo la modifica
	 * @throws IOException
	 */
	public static void modificaFileUsati(String titolo, String autore, String anno, String genere, String inPrestito, String nClienteLibro, String inPrestitoNuovo, String possiedeLibroNuovo) throws IOException {

		try {
			// apro il file
			accediLettura("usati.txt");
			fileContent = new StringBuilder();

			// Leggo il file riga per riga
			while ((strLine = br.readLine()) != null) {


				if(strLine.equals(titolo + ";" + autore + ";" + anno + ";" + genere + ";" + inPrestito + ";" + nClienteLibro + ";")){
					// se la riga e uguale a quella ricercata
					fileContent.append(titolo + ";" + autore + ";" + anno + ";" + genere + ";" + inPrestitoNuovo + ";" + possiedeLibroNuovo + ";" +System.getProperty("line.separator"));

				} else {
					// ... altrimenti la trascrivo cosi com'e
					fileContent.append(strLine);
					fileContent.append(System.getProperty("line.separator"));
				}
			}

			// Sovrascrivo il file con il nuovo contenuto (aggiornato)
			sovrascrivo("usati.txt", fileContent);
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			// chiusura dell'output e dell'input
			chiudoStream();

		}


	}
	
	/**
	 * Chiusura dell'output e dell'input
	 */
	private static void chiudoStream() {
		try {
			fstream.close();
			out.flush();
			out.close();
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Sovrascrivo il file con il nuovo contenuto (aggiornato)
	 * @param strPathFile File da aggiornare
	 * @param datiAggiornati  
	 * @return BufferedWriter
	 * @throws IOException
	 */
	private static BufferedWriter sovrascrivo(String strPathFile,StringBuilder datiAggiornati) throws IOException {
		FileWriter fstreamWrite = new FileWriter(strPathFile);
		out = new BufferedWriter(fstreamWrite);
		out.write(datiAggiornati.toString());
		return out;
	}
	
	/**
	 * Metodo per aprire il file
	 * @param strPathFile File da leggere
	 * @return BufferedReader
	 * @throws FileNotFoundException
	 */
	private static BufferedReader accediLettura(String strPathFile) throws FileNotFoundException {
		// TODO Auto-generated method stub
		fstream = new FileInputStream(strPathFile);//ti taglierei la gola

		// prendo l'inputStream
		in = new DataInputStream(fstream);
		br = new BufferedReader(new InputStreamReader(in));
		return br;
	}


}