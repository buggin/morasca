import java.awt.*;
import java.awt.event.*;
import java.io.IOException;

import javax.swing.*;

public class Registrazione extends JFrame {

	private JTextField numTel2;
	private JLabel numTel1;
	private JButton conferma, annulla;

	private Container container;
	private GridBagLayout tabella;
	private GridBagConstraints constraints;

	public Registrazione () {

		super ("Registrazione");

		container = getContentPane();
		tabella = new GridBagLayout ();
		container.setLayout(tabella);
		constraints = new GridBagConstraints();

		constraints.fill = GridBagConstraints.BOTH;
		constraints.weightx = 1000;
		constraints.weighty = 1000;

		numTel1 = new JLabel("   Inserire il Numero di Telefono ");              // FISICAMENTE CORRISPONDE ALLA CONSEGNA DELLA TESSERA E ALLA LETTURA DELLA STESSA TRAMITE UN LETTORE DI TESSERE (LA TESSERA E' MAGNETICA)
		numTel1.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		numTel2 = new JTextField("");
		numTel2.setEditable(true);
		numTel2.setFont(new Font( "Serif", Font.PLAIN, 20 ) );

		conferma = new JButton("Conferma");
		conferma.setFont(new Font( "Serif", Font.PLAIN, 15 ) );

		annulla = new JButton("Annulla");
		annulla.setFont(new Font( "Serif", Font.PLAIN, 15 ) );

		// ADDCOMPONENT

		addComponent (numTel1,       0, 0, 2, 1);
		addComponent (numTel2,       0, 2, 2, 1);
		addComponent (annulla,           1, 2, 1, 1);
		addComponent (conferma,            1, 3, 1, 1);

		// EVENTI



		annulla.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						setVisible( false ); 
					}
				}
				);

		conferma.addActionListener(
				new ActionListener() {
					public void actionPerformed( ActionEvent event ) {
						boolean can = true;
						if (!( numTel2.getText().equals(""))) {
							try {
								long l = Long.parseLong(numTel2.getText());
							}
							catch (java.lang.NumberFormatException e) {
								JOptionPane.showMessageDialog( Registrazione.this, "Errore nella compilazione!", "", JOptionPane.PLAIN_MESSAGE);
								can = false;
							}
							if (can) {
								try {
									Libreria.registra(numTel2.getText());
								} catch (IOException e) {
								}
								String s = Integer.toString(Libreria.numClienti - 1);
								JOptionPane.showMessageDialog( Registrazione.this, "Registrazione avvenuta - Consegnare la Tessera al Cliente numero " + s, "Registrazione avvenuta", JOptionPane.PLAIN_MESSAGE );
								Display.logout.setVisible(true);
								String b = Integer.toString(Display.clienteincorso.bonus);
								Display.libri1.setText(" Libri al prossimo sconto:    " + b);
								Display.conferma.setText("Conferma");
								Display.iconaLucchetto.setVisible(true);
								Display.iconaLucchetto.setText("Cliente " + Display.clienteincorso.numTessera);
								Display.libri1.setVisible(true);
								Display.accedi.setVisible(false);
								Display.registrazione.setVisible(false);
								setVisible(false);

							}
						}
						else {
							JOptionPane.showMessageDialog( Registrazione.this, "Errore nella compilazione!", "Operazione non riuscita", JOptionPane.PLAIN_MESSAGE);
						}

					}
				}
				);

		setResizable( false );
		setSize( 700, 100 );
		setLocationRelativeTo(null);
		setVisible( true );

	}   // END CONSTRUCTOR

	//METODO ADDCOMPONENT

	private void addComponent( Component component, int row, int column, int width, int height ) {

		constraints.gridx = column;
		constraints.gridy = row;

		constraints.gridwidth = width;
		constraints.gridheight = height;

		tabella.setConstraints( component, constraints );
		container.add( component ); 
	}


} // END CLASS