import java.io.FileNotFoundException;
import java.io.IOException;

public class Libreria {

	protected static Nuovo[] listalibri = new Nuovo[100000];         // LIBRI DA VENDERE
	protected static Usato[] usati = new Usato[1000];                // LIBRI DEL SERVIZIO BIBLIOTECA
	protected static Cliente[] clienti = new Cliente[100000];        // CLIENTI REGISTRATI

	protected static int numLibri, numUsati, numClienti;
	
	/**
	 * Ricerca il libro passato come parametro nella lista dei libri e ritorna la stringa che ne contiene le informazioni 
	 * @param titolo Titolo del libro di cui volgiamo le informazioni
	 * @return La stringa contenente le informazioni del libro (se non esiste ritorna la stringa "0")
	 */
	public static String infoLibro ( String titolo) {
		
		for (int i = 0; i < numLibri; i++) {
			if (listalibri[i].titolo.equals(titolo)) {
				String ret = "Titolo:    " + listalibri[i].titolo + "\n" + "Autore:   " + listalibri[i].autore + "\n" + "Anno:     " + listalibri[i].anno + "\n" + "Genere:   " + listalibri[i].genere + "\n" + "Copie:     " + listalibri[i].numCopie + "\n" + "Prezzo:   " + listalibri[i].prezzo;
                return ret;
			}
		}
		return "0";

	}
	
/**
 * 	Ricerca il libro passato come parametro nella lista dei libri e ritorna la stringa che ne contiene le informazioni 
 * @param titolo Titolo del libro di cui vogliamo le informazioni
 * @return La stringa contenente le informazioni del libro (se non esiste ritorna la stringa "0")
 */
public static String infoUsato ( String titolo) {
		
		for (int i = 0; i < numUsati; i++) {
			if (usati[i].titolo.equals(titolo)) {
				String s, s1;
				if (usati[i].inPrestito == 0) {
					s = "NO"; }
				else {
					s = "SI";
				}
				if (usati[i].inPrestito == 0) {
					s1 = "";
				}
				else {
					s1 = "Dal cliente      " + usati[i].possiedeLibro;
				}
				String ret = "Titolo:           " + usati[i].titolo + "\n" + "Autore:          " + usati[i].autore + "\n" + "Anno:            " + usati[i].anno + "\n" + "Genere:          " + usati[i].genere + "\n" + "In prestito:     " + s + "\n" + s1 ;
                return ret;
			}
		}
		return "0";

	}
	
	
	

	/**
	 * Aggiunge un libro nuovo nella lista dei libri e scrive sul file di testo nuovi.txt
	 * @param libro Libro che stiamo aggiungendo
	 * @throws FileNotFoundException
	 */
	public static void addLibro (Nuovo libro) throws FileNotFoundException {       // SOLO NEL CASO IN CUI ARRIVA UN LIBRO CHE NON HO MAI AVUTO, ALTRIMENTI E' UN RIFORNIMENTO

		listalibri[numLibri] = libro;
		numLibri ++;

		// SCRITTURA DEL LIBRO NEL FILE DI TESTO
		String prezzo = String.valueOf(libro.prezzo);
		String ncopie = String.valueOf(libro.numCopie);
		Scrittura.aggiungiNuovo(libro.titolo, libro.autore, libro.anno, libro.genere, prezzo, ncopie); 
	}

	/**
	 * Aggiunge un libro usato nella lista dei libri e scrive sul file di testo usati.txt
	 * @param libro Libro usato che stiamo aggiungendo
	 * @return Ritorna 0 se va a buon fine, 1 se ho gia questo libro tra i libri usati
	 * @throws FileNotFoundException
	 */
	public static int addUsato (Usato libro) throws FileNotFoundException {           // SOLO SE NON CE L'HO GIA' TRA I LIBRI USATI

		for (int i = 0; i < numUsati; i++) {
			if (usati[i].titolo.equals(libro.titolo)) {
				return 1;                                // HO GIA' QUESTO LIBRO USATO, NON LO AGGIUNGO
			}
		}

		usati[numUsati] = libro;            
		numUsati++;  
		
		//SCRITTURA DEL LIBRO NEL FILE DI TESTO
		Scrittura.aggiungiUsato(libro.titolo, libro.autore, libro.anno, libro.genere, "0", "-1");
		
		return 0;                                     // OPERAZIONE COMPLETATA
	}

	/**
	 * Cerca un libro nella lista dei libri nuovi
	 * @param titolo Titolo del libro che si sta cercando
	 * @return Ritorna 0 se il libro e disponibile, 1 se non ci sono copie in libreria e 2 se il libro non esiste
	 */
	public static int cercaLibro (String titolo) {

		for (int i = 0; i < numLibri; i++) {
			if (listalibri[i].titolo.equals(titolo)) {

				if ( listalibri[i].numCopie > 0 ) {            
					Display.libroincorso = listalibri[i];
					return 0;
				}
				else {
					// SONO IN ATTESA DEL RIFORNIMENTO  ( CHE HO GIA' CHIAMATO )
					return 1;                        // --> NON CI SONO COPIE IN LIBRERIA, PASSARE TRA QUALCHE GIORNO
				}
			} 
		}
		return 2;                // IL LIBRO NON E' PRESENTE NELLA LISTA DELLA LIBRERIA ( E NON ARRIVERA' )
	}

	/**
	 * Chiama il rifornimento del libro passsato come parametro
	 * @param libro Libro di cui vogliamo un rifornimento
	 */
	public static void rifornimento (Nuovo libro) {                       // CHIAMATA RIFORNIMENTO
		for (int i = 0; i < numLibri; i++) {
			if (listalibri[i].titolo.equals(libro.titolo)) {
				// CHIAMA RIFORNIMENTO ( MAIL ALLA CASA EDITRICE O ALTRO )
			}
		}
	}


	/**
	 * Modifica il numero di copie relative a un libro in seguito a un rifornimento arrivato e scrive sul file di testo nuovi.txt
	 * @param titolo Titolo del libro
	 * @param numeroCopie Numero di copie arrivate
	 * @return Ritorna 0 se l'operazione va a buon fine, 1 se il libro non esiste
	 */
	public static int rifornimentoArrivato (String titolo, int numeroCopie) {               // RIFORNIMENTO ARRIVATO
		for (int i = 0; i < numLibri; i++) {
			if (listalibri[i].titolo.equals(titolo)) {
				listalibri[i].numCopie = numeroCopie;

				// MODIFICO IL NUMERO DI COPIE SUL FILE DI TESTO

				String prezzo = String.valueOf(listalibri[i].prezzo);
				String numCopie = String.valueOf(listalibri[i].numCopie);
				String anno = String.valueOf(listalibri[i].anno);
				try {
					Scrittura.modificaFileNuovi(listalibri[i].titolo, listalibri[i].autore, anno, listalibri[i].genere, prezzo, "0", numCopie);
				} catch (IOException e) {}
				return 0;                                    // OPERAZIONE COMPLETATA
			}
		}
		return 1;                            // IL LIBRO NON ESISTE
	}

	/**
	 * Da un lbro in prestito
	 * @param titolo Titolo del libro che vogliamo dare in prestito
	 * @return Ritorna 1 se il libro e gia in prestito, 0 se e disponibile
	 */
	public static int prestaLibro (String titolo) {

		for (int i = 0; i < numUsati; i++) {
			if (usati[i].titolo.equals(titolo)) {

				if (usati[i].inPrestito == 1) {
					Display.usatoincorso = usati[i];
					return 1;                           // IL LIBRO E' GIA' IN PRESTITO
				}
				else {                                  // PRESTITO DISPONIBILE
					Display.usatoincorso = usati[i];     
					return 0;
				}
			}

		}
		return 2;   // IL LIBRO NON E' PRESENTE NELLA LISTA DEI LIBRI USATI
	}


	/**
	 * Metodo chiamato in seguito alla restituzione di un libro usato
	 * @param titolo Titolo del libro restituito
	 * @return Ritorna 0 se l'operzione va a buon fine, 1 se il libro non era in prestito, 2 se il libro non esiste
	 */
	public static int restituzioneLibro( String titolo) {

		for (int i = 0; i < numUsati; i++) {
			if (usati[i].titolo.equals(titolo)) {
				
				if (usati[i].inPrestito == 1) {
					String s = String.valueOf(usati[i].possiedeLibro);
					usati[i].inPrestito = 0;
					usati[i].possiedeLibro = -1;
					
					// AGGIORNO IL FILE DI TESTO
					
					try {
						Scrittura.modificaFileUsati(usati[i].titolo, usati[i].autore, usati[i].anno, usati[i].genere, "1", s, "0", "-1");
					} catch (IOException e) {}
					
					return 0;                             // OPERAZIONE COMPLETATA
				}
				else {
					return 1;                            // IL LIBRO NON ERA IN PRESTITO
				}
			}
		}

		return 2;                                         // IL LIBRO NON ESISTE TRA I LIBRI USATI
	}

	/**
	 * Registra un nuovo cliente nella lista clienti e aggiorna il file di testo clienti.txt
	 * @param numeroTelefono Numero di telefono del cliente
	 * @throws IOException
	 */
	public static void registra(String numeroTelefono) throws IOException {                   

		Cliente nuovo = new Cliente (numClienti, numeroTelefono);
		clienti[numClienti] = nuovo;
		numClienti++;
		Display.clienteincorso = nuovo;

		//SCRITTURA SU FILE CLIENTI
		Scrittura.iscriviCliente(nuovo.numTessera, nuovo.bonus, numeroTelefono);

		return;                                 // REGISTRAZIONE AVVENUTA
	}

	/**
	 * Effettua il login di un cliente
	 * @param numeroTessera
	 * @return Ritorna il cliente che ha effettato l'accesso
	 */
	public static Cliente accedi (int numeroTessera) {

		Cliente c = clienti[numeroTessera];                    // L'UTENTE CON LA TESSERA NUMERO 23 SI TROVA NELLA POSIZIONE clienti[23]
		return c;
	}

	/**
	 * Effettua il logout 
	 */
	public static void logout () {

		Display.clienteincorso = null;
		Display.libri1.setVisible(false);
		
		return;
	}

}   // END CLASS