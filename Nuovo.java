public class Nuovo extends Libro {
	
	protected double prezzo;
	protected int numCopie;
	
	public Nuovo (String t, String a, String an, String g, int nc, double p) {
		super ( t, a, an, g);
		this.prezzo = p;
		numCopie = nc;
	}
}